<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
/*
 * IndexINC | Versión 0.1
 * Algoritmo, PHP empotrado,para la indexación de directorios y archivos en un servidor.
 * permite ingresar información como tamaño, nombre, tipo, fecha, etc. a
 * la base de datos y permitir comparar entre archivos.
 * No se ofrece garantia sobre el uso de este software.
 * Licencia GPL
 * 21/08/2009
 * 24/08/2009
 *
 */

//variables globales : Fuente www.php.net
if(!ini_get('register_globals')){
    $superglobales = array($_SERVER, $_ENV,
    $_FILES, $_COOKIE, $_POST, $_GET);
    if(isset($_SESSION)){
        array_unshift($superglobales, $_SESSION);
    }
    foreach($superglobales as $superglobal){
        extract($superglobal, EXTR_SKIP);
    }
}



//Esta función nos devolverá el tamaño del los archivos y/o directorios
function size($path,$formated=true,$retstring=null)
{
    if(!is_dir($path) || !is_readable($path))
    {
        if(is_file($path) || file_exists($path))
        {
            $size = filesize($path);
        }else{
            return false;
        }

    }else{
        $path_stack[]=$path;
        $size=0;
        do{
            $path=array_shift($path_stack);
            $handle = opendir($path);
            while(false !== ($file = readdir($handle))){
                if($file != '.' && $file != '..' && is_readable($path . DIRECTORY_SEPARATOR . $file)){
                    if(is_dir($path . DIRECTORY_SEPARATOR . $file)){ $path_stack[] = $path . DIRECTORY_SEPARATOR . $file;
                    }
                    $size += filesize($path . DIRECTORY_SEPARATOR . $file);
                }
            }
            closedir($handle);
        }while(count($path_stack)>0);
    }
    
    if($formated)
    {
        $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        if($retstring == null) { $retstring = '%01.2f %s'; }
        $lastsizestring = end($sizes);
        foreach($sizes as $sizestring){
            if($size <1024){ break; }
            if($sizestring != $lastsizestring){ $size /= 1024; }
        }
        
        if($sizestring == $sizes[0])
        {
            $retstring = '%01d %s';
        } // los Bytes normalmente no son fraccionales
        
        $size = sprintf($retstring, $size, $sizestring);
    }

    return $size;
}

//Funcionar para explorar directorios
function explorar($dir)
{
    $dir=(isset($_GET['dir']))?$_GET['dir']:"ejemplos";
    if($dir=="../" || $dir=="" || $dir==".../" || $dir=="."){
        echo "No esta permitido revisar este nivel";
    }else{
        $directorio=opendir($dir);
        $size_dir=size($dir);
        //$dir_superenlace="$dir";
        echo "<b>Directorio actual:</b><br><a href=\"?dir=$dir\">$dir</a> | tamaño: <b>$size_dir</b><br>";
        //echo "<b>Directorio actual:</b><br>$dir | tamaño: <b>$size_dir</b><br>";
        echo "<b>Archivos:</b><br>";
        while($archivo = readdir($directorio)){
            if($archivo == '.')
                echo "<a href=\"?dir=.\">$archivo</a><br>";
            elseif($archivo == '..'){
                if($dir != '.'){
                    $carpetas = split("/",$dir);
                    array_pop($carpetas);
                    $dir2 = join("/",$carpetas);
                    echo "<a href=\"?dir=$dir2\">$archivo</a><br>";
                }
            }
            if($archivo == '.' || $archivo == '..' || $archivo == ''){
                $estado="TRUE"; $alerta="TRUE";
            }elseif(is_dir("$dir/$archivo")){
                $pathdir="$dir/$archivo";
                $size_dir=size($pathdir);
                echo " <b>CARPETA</b> <a href=\"?dir=$pathdir\">$archivo</a> | <u>tamaño</u>: <b>$size_dir</b><br> ";
            }else{
                $patharchivo="$dir/$archivo";
                $size_archivo=size($patharchivo);
                //echo " <b>ARCHIVO</b> <a href=\"$patharchivo\">$archivo</a> | <u>tamaño</u>: <b>$size_archivo</b><br> ";
                echo " <b>ARCHIVO</b> $archivo | <u>tamaño</u>: <b>$size_archivo</b><br> ";
            }
        }
        closedir($directorio);
    }
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>indexINC</title>
    </head>
    <body>
        <div align="left">
        <?php
        $_GET[dir]=$dir;
        explorar($dir);
        echo "<br>";
        #phpinfo();
        ?>
        </div>
    </body>
</html>
